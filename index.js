'use strict'

const express = require ('express')
const bodyParser = require ('body-parser')
const mongoose = require('mongoose')

const Product = require('./models/products')

const app = express ()
const port =process.env.PORT || 3001

app.use(bodyParser.urlencoded ({ extended: false}))
app.use(bodyParser.json())

app.get('/api/product', (req,res) =>{

    Product.find({}, (err, products) => {
        if(err) return res.status(500).send({ message: `Error al realizar la peticion ${err}`})
        if(!products) return res.status(404).send({ message: `No existen productos`})
        
        res.status(200).send({products});
    })
}) 

app.get('/api/product/:productId', (req,res) =>{
    let productId = req.params.productId

    Product.findById(productId, (err, product) => {
        if (err) return res.status(500).send({message:`Error al realizar la peticion: ${err}`})
        if (!product) return res.status(404).send({message: `El producto no existe`})
   
        res.status(200).send({ product })

})
})

app.post('/api/product', (req, res) =>{
    console.log('POST /api/product')
    console.log(req.body)

    let product = new Product()
    product.name =req.body.name
    product.picture =req.body.name
    product.price = req.body.price
    product.category = req.body.category
    product.description = req.body.description

    product.save((err, productStored) => {
     if (err) res.status(500).send({message:`Error al salvar en la BBDD: ${err}`})
    
        res.status(200).send({product: productStored})
      
    })
})

app.put('/api/product/:productId', (req, res) => {

    let productId = req.params.productId
    let update = req.body

    Product.findByIdAndUpdate(productId, update, (err, productUpdate) =>{
        if(err) return res.status(500).send({ message: `Error al actualizar un producto: ${err}`})

        res.status(200).send({ product: productUpdate})
    })
    
    })


app.delete('/api/product/:productId', (req, res) => {
    let productId = req.params.productId

    Product.findById(productId, (err, product) => {
        if(err) return res.status(500).send({ message: `Error al borrar producto: ${err}`})

        product.remove(err => {
            if(err) return res.status(500).send({ message: `Error al borrar producto: ${err}`})
            res.status(200).send({ message: `El producto ha sido eliminado`})
        })
    })
})

app.get('/hola/:name', (req, res) => {
res.send({message: `hola ${req.params.name}!`})
})

//CONEXION AZURE
//mongoose.connect('mongodb://litiobyte:OKRgWFmAUDhGe16KiAMXIlYaWJIudsostXClJFEsFt0UV4DT60lPjG4RyYEyl3TE0OE3wxR66G82ENnuNGWryA%3D%3D@litiobyte.documents.azure.com:10255/?ssl=true', (err, res) => {
//CONEXION MLab

    mongoose.connect('mongodb://dchandia:Blackswan01@ds249249.mlab.com:49249/lbexperimental', (err, res) => {


        if (err) throw err
        console.log ('Conexion establecida a la base de datos')

        app.listen(port, () => {
            console.log(`API REST corriendo en http://localhost:${port}`)

})

})